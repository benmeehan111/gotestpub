package greetings

import (
	"fmt"

	"github.com/gorilla/mux"
)

// Hello returns a greeting for the named person.
func Hello(name string) string {
	// Return a greeting that embeds the name in a message.
	r := mux.NewRouter()
	fmt.Println(r)
	message := fmt.Sprintf("Hi, %v. Welcome!", name)
	return message
}
